﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class RangeDrawer : MonoBehaviour
{

    public int polysInCone = 1;

    SpriteRenderer hearingRend;
    Mesh mesh;
    MeshRenderer meshRend;

    float oldHR;
    float oldVR;
    float oldVCD;


    private void Start()
    {
        hearingRend = transform.GetChild(0).GetComponent<SpriteRenderer>();
        meshRend = transform.GetChild(1).GetComponent<MeshRenderer>();
        if(PrefabUtility.GetCorrespondingObjectFromSource(gameObject) != null)
            mesh = transform.GetChild(1).GetComponent<MeshFilter>().mesh = new Mesh();

        float oldOldHR = oldHR;
        float oldOldVR = oldVR;
        float oldOldVCD = oldVCD;

        oldHR++;
        oldVR++;
        oldVCD++;

        UpdateRanges(oldOldHR, oldOldVR, oldOldVCD);
    }

    public void UpdateRanges(float hearingRadius, float visionRadius, float visionConeDegrees)
    {
  
        if(mesh != null)
        {
            if (oldHR != hearingRadius)
            {
                oldHR = hearingRadius;

                hearingRend.transform.localScale = hearingRadius * Vector2.one * 2;
            }

            if ((oldVR != visionRadius || oldVCD != visionConeDegrees))
            {
                oldVR = visionRadius;
                oldVCD = visionConeDegrees;


                if (polysInCone < 1)
                    polysInCone = 2;




                mesh.Clear();


                Vector3[] vertices = new Vector3[polysInCone + 2];
                int[] triangles = new int[(polysInCone) * 3];

                vertices[0] = Vector3.zero;

                for (int i = 0; i < polysInCone + 1; i++)
                {
                    float angleInRad = (i * (visionConeDegrees / polysInCone) - visionConeDegrees/2) * Mathf.PI / 180;



                    vertices[i + 1] = new Vector3(Mathf.Cos(angleInRad), Mathf.Sin(angleInRad), 0) * visionRadius;
                    //Debug.Log(angleInRad + " " + Mathf.Cos(angleInRad));

                    if (i > 0)
                    {
                        triangles[(i - 1) * 3] = 0;
                        triangles[(i - 1) * 3 + 1] = i + 1;
                        triangles[(i - 1) * 3 + 2] = i;
                    }

                }
                mesh.vertices = vertices;
                mesh.triangles = triangles;

                mesh.RecalculateNormals();

                mesh.MarkModified();

                meshRend.sortingLayerName = "Overlay";
            }
        }
        else
        {
            if(PrefabUtility.GetCorrespondingObjectFromSource(gameObject) != null)
            {
                oldHR = hearingRadius;
                oldVR = visionRadius;
                oldVCD = visionConeDegrees;
            }

        }

        
        
    }


}
