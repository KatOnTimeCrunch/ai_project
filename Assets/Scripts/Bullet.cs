﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public Creep target;
    public float speed;
    public int damage;
   
    void FixedUpdate()
    {
        if (target)
        {
            Vector2 delta = target.transform.localPosition - transform.localPosition;
            if (delta.sqrMagnitude > speed * speed)
            {
                transform.localPosition += (Vector3)delta.normalized * speed;
            }
            else
            {
                target.DamageCreep(damage);
                Destroy(gameObject);
            }
        }
        else
        {
            Destroy(gameObject);
        }
        
    }
}
