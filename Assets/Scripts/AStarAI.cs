﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AStarAI : MonoBehaviour
{
    public Vector2 target;
    public int tilesWidth = 20;
    public float distMult = 1f;
    public float speed = 1;
    List<Vector2> path;

    float[] tiles;
    List<int> tilesToOpen;
    Vector3 pathTargetDir;
    int tileSize = 1;
    float distLeft;
    bool pathExists;



    void Start()
    {

        GenerateMapTerrain();
        GenerateMapPathFromTerrain(target);
        path = MakePathFromMap(target);


        Print();

        pathExists = path.Count > 0;
        if (pathExists)
        {
            pathTargetDir = path[0];
            path.RemoveAt(0);
        }
        

    }

    int timer = 60;
    void FixedUpdate()
    {
        if (timer <= 0 && pathExists)
        {
            float movementLeft = speed;

            if (transform.position != (Vector3)target)
                while (movementLeft > 0)
                {
                    if (movementLeft < distLeft)
                    {
                        distLeft -= movementLeft;
                        transform.position += (Vector3)pathTargetDir * movementLeft;
                        break;
                    }
                    else
                    {
                        movementLeft -= distLeft;
                        transform.position += pathTargetDir * distLeft;

                        if (path.Count > 0)
                        {
                            pathTargetDir = (Vector3)path[0];
                            distLeft = tileSize;

                            path.RemoveAt(0);
                        }
                        else
                        {
                            pathTargetDir = ((Vector3)target - transform.position).normalized;
                            distLeft = ((Vector3)target - transform.position).magnitude;
                        }
                    }
                    if (transform.position == (Vector3)target)
                        break;
                }
        }
        else
            timer--;


    }

    private void Print()
    {
        string s = "";
        for (int y = 0; y < tilesWidth; y++)
        {

            for (int x = 0; x < tilesWidth; x++)
            {
                string sp = tiles[x + y * tilesWidth].ToString();

                s += $"{sp} ";
                if (sp.Length == 1)
                    s += " ";

            }
            s += "\n";
        }
        Debug.Log(s);

    }

    private void GenerateMapTerrain()
    {
        tilesToOpen = new List<int>();
        tiles = new float[tilesWidth * tilesWidth];
        for (int i = 0; i < tiles.Length; i++)
        {
            var hits = Physics2D.BoxCastAll(TileToVector(i), Vector2.one * tileSize, 0, Vector2.zero, 0);
            foreach (var hit in hits)
            {
                if (hit.transform != transform)
                {
                    tiles[i] = -1;
                    break;
                }

            }



        }

    }

    public void GenerateMapPathFromTerrain(Vector2 target)
    {
        int startingTile = VectorToTile(transform.position);
        int targetTile = VectorToTile(target);
        tiles[startingTile] = GetDistance(startingTile, targetTile) * distMult;
        tilesToOpen.Add(startingTile);
        while (tilesToOpen.Count > 0 && tilesToOpen[0] != targetTile)
        {
            int tile = tilesToOpen[0];
            tilesToOpen.RemoveAt(0);
            OpenTile(tile, targetTile);

        }
    }

    public List<Vector2> MakePathFromMap(Vector2 target)
    {

        int targetTile = VectorToTile(target);
        int tile = targetTile;
        List<Vector2> path = new List<Vector2>();



        for (int i = 0; i < tiles[targetTile]; i++)
        {
            int bestTileId = 0;
            float shortestDistanceToStart = float.PositiveInfinity;

            for (int dir = 0; dir < 2; dir++)
            {
                for (int dist = 0; dist < 2; dist++)
                {
                    int tileToCheck = 0;

                    //dir 0 is x, 1 is y
                    if (dir == 0)
                        tileToCheck = AddToTileX(tile, dist * 2 - 1);
                    else
                        tileToCheck = AddToTileY(tile, dist * 2 - 1);


                    if (tileToCheck != -1 && tiles[tileToCheck] > 0 && tiles[tileToCheck] - GetDistance(tileToCheck, targetTile) * distMult < shortestDistanceToStart)
                    {

                        bestTileId = tileToCheck;
                        shortestDistanceToStart = tiles[tileToCheck] - GetDistance(tileToCheck, targetTile) * distMult;
                    }
                }
            }
           
            //y dir is inverted
            path.Add(new Vector2(GetTileX(tile) - GetTileX(bestTileId), GetTileY(bestTileId) - GetTileY(tile)));
            tile = bestTileId;
        }
        path.Reverse();
        return path;
    }

    private void OpenTile(int tile, int targetTile)
    {
        float tileDistanceFromStart = tiles[tile] - GetDistance(tile, targetTile) * distMult;
        for (int dir = 0; dir < 2; dir++)
        {
            for (int dist = 0; dist < 2; dist++)
            {
                int tileToCheck = 0;

                //dir 0 is x, 1 is y
                if (dir == 0)
                    tileToCheck = AddToTileX(tile, dist * 2 - 1);
                else
                    tileToCheck = AddToTileY(tile, dist * 2 - 1);


                //-1 is the error code for AddToTileX/Y
                if(tileToCheck != -1)
                {
                    float tileValue = tileDistanceFromStart + GetDistance(tileToCheck, targetTile) * distMult + 1;
                    if (tileValue < tiles[tileToCheck] || tiles[tileToCheck] == 0)
                    {

                        tiles[tileToCheck] = tileValue;

                        for (int i = 0; i < tilesToOpen.Count + 1; i++)
                        {
                            if (i == tilesToOpen.Count)
                            {
                                if (!tilesToOpen.Contains(tileToCheck))
                                    tilesToOpen.Add(tileToCheck);
                                break;
                            }

                            if (tiles[tilesToOpen[i]] > tiles[tileToCheck])
                            {
                                if (tilesToOpen.Contains(tileToCheck))
                                    tilesToOpen.Remove(tileToCheck);
                                tilesToOpen.Insert(i, tileToCheck);
                                break;
                            }
                        }

                    }
                }
                    

                

            }
        }
    }

    private int GetDistance(int tileA, int tileB)
    {
        return Math.Abs(GetTileX(tileA) - GetTileX(tileB)) + Math.Abs(GetTileY(tileA) - GetTileY(tileB));
    }

    private bool TileInBounds(int tile)
    {
        return tile >= 0 && tile < tiles.Length;
    }

    private int VectorToTile(Vector2 vector)
    {
        return ((int)(vector.x - (float)tileSize / 2) - (int)(vector.y + (float)tileSize / 2) * tilesWidth) / tileSize + (tilesWidth + tiles.Length) / 2;
    }
    private Vector2 TileToVector(int tile)
    {
        return new Vector2(GetTileX(tile - tilesWidth / 2) * tileSize + (float)tileSize/2, GetTileY(tiles.Length / 2 - tile) * tileSize - (float)tileSize / 2);
    }

    private int GetTileX(int tile)
    {
        return tile % tilesWidth;
    }
    private int GetTileY(int tile)
    {
        return tile / tilesWidth;
    }
    private int AddToTileX(int tile, int xToAdd)
    {
        int newTile = tile + xToAdd;
        if (GetTileY(newTile) == GetTileY(tile))
            return newTile;
        return -1;
    }
    private int AddToTileY(int tile, int yToAdd)
    {
        int newTile = tile + yToAdd * tilesWidth;
        if (TileInBounds(newTile))
            return newTile;
        return -1;
    }
}
