﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class World : MonoBehaviour
{

    public Vector2Int size;
    public Sprite background;
    public HeadQuarters headQuarters;
    public CreepManager creepManager;
    Dictionary<Vector2Int, Building> tiles;

    Grid grid;





    private void Start()
    {
        grid = GetComponent<Grid>();
        tiles = new Dictionary<Vector2Int, Building>();

        for (int x = -size.x / 2; x < size.x / 2; x++)
        {
            for (int y = -size.y / 2; y < size.y / 2; y++)
            {
                tiles.Add(new Vector2Int(x, y), null);
            }
        }
        InitBackground();
        InitHQ();


    }
    private void InitBackground()
    {
        GameObject obj = new GameObject("Background");
        obj.transform.parent = transform;
        obj.transform.position = Vector3.zero;
        SpriteRenderer rend = obj.AddComponent<SpriteRenderer>();
        rend.sprite = background;
        rend.sortingLayerName = "Background";
        rend.drawMode = SpriteDrawMode.Tiled;
        rend.size = size;
    }



    private void InitHQ()
    {
        SetTile(GetTilePos(headQuarters), headQuarters);
    }

    public void SetTile(Vector2Int pos, Building building)
    {
        if (InBounds(pos))
        {
            tiles[pos] = building;
            if(building != null)
            {
                building.transform.parent = transform;
                building.transform.localPosition = pos * (Vector2)grid.cellSize;
            }

            if (creepManager.shortestPathToHeadQuarters?.ContainsKey(pos) ?? false)
            {
                creepManager.GeneratePathMap();
            }
               
        }

    }

    public Building GetBuilding(Vector2Int tileToCheck)
    {
        tiles.TryGetValue(tileToCheck, out Building b);
        return b;
    }
    public Vector2Int GetTilePos(Building building)
    {
        return GetTilePos(building.transform);
    }

    public Vector2Int GetTilePos(Transform trans)
    {
        Vector2 posNonRounded = (Vector2)trans.position / grid.cellSize;
        return new Vector2Int(Mathf.RoundToInt(posNonRounded.x), Mathf.RoundToInt(posNonRounded.y));
    }

    public bool InBounds(Vector2Int tileToCheck)
    {
        return tiles.ContainsKey(tileToCheck);
    }

    public int SellBuilding(Vector2Int pos)
    {
        Building b = GetBuilding(pos);
        int cost = 0;
        if (b != null)
            cost = b.cost;
        Destroy(b.gameObject);
        SetTile(pos, null);
        return cost;
    }
}

