﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIManager : MonoBehaviour
{
    public BuildingPlacer buildingPlacer;
    public LayoutGroup buildingSelect;
    public Button startButton;
    public Button buttonPrefab;
    public CreepManager creepManager;

    public List<Building> buildingList;

    public void Start()
    {
        startButton.onClick.AddListener(creepManager.StartWave);
        foreach (Building building in buildingList)
        {
            Button button = Instantiate(buttonPrefab);
            button.transform.SetParent(buildingSelect.transform);
            button.GetComponentInChildren<Text>().text = building.name;
            button.onClick.AddListener(() => buildingPlacer.SetBuilding(building));
        }
    }
}
