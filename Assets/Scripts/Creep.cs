﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creep : MonoBehaviour
{
    public float speed;
    public int damage;
    public int health;
    public CreepManager creepManager;
    Vector2Int currentTile;
    void Start()
    {
        currentTile = creepManager.world.GetTilePos(transform);
    }

    void FixedUpdate()
    {
        
        Vector2 delta = creepManager.shortestPathToHeadQuarters[currentTile] - (Vector2)transform.localPosition;
        if(delta.sqrMagnitude > speed * speed)
        {
            transform.localPosition += (Vector3)delta.normalized * speed;
        }
        else
        {
            transform.localPosition = (Vector2)creepManager.shortestPathToHeadQuarters[currentTile];
            currentTile = creepManager.shortestPathToHeadQuarters[currentTile];
            if(currentTile == creepManager.world.GetTilePos(creepManager.world.headQuarters))
            {
                creepManager.world.headQuarters.Damage(damage);
                creepManager.KillCreep(this);
            }
        }
    }

    public void DamageCreep(int amount)
    {
        health -= amount;
        if (health <= 0)
            creepManager.KillCreep(this);
    }
}
