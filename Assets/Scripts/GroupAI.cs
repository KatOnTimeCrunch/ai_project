﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UIElements;

public class GroupAI : MonoBehaviour
{
    public float hearingRadius;
    public float visionRadius;
    public float visionConeDegrees;

    public float groupRuleWeight = 1;
    public float distanceingRuleWeight = 5;
    public float sameDirRuleWeight = 1;




    float preferedDistanceToGroupMember = 5f;


    public float speed = 0.05f;

    float radius;
    Vector2 lookingDir;

    List<GroupAI> agentsInRange;

    void Start()
    {
        agentsInRange = new List<GroupAI>();
        GetComponent<RangeDrawer>().UpdateRanges(hearingRadius, visionRadius, visionConeDegrees);
        radius = GetComponent<CircleCollider2D>().radius;

        lookingDir = new Vector2(Mathf.Cos(transform.localEulerAngles.z * Mathf.PI / 180), Mathf.Sin(transform.localEulerAngles.z * Mathf.PI / 180));
    }


    void FixedUpdate()
    {
        UpdateSenses();

        Vector2 walkingDir = Vector2.zero;

        foreach (var agent in agentsInRange)
        {
            Vector2 delta = agent.transform.position - transform.position;

            float dist = (delta.magnitude - (radius + agent.radius));
            walkingDir += delta.normalized * dist * groupRuleWeight;

            if(dist < preferedDistanceToGroupMember)
            {
                walkingDir -= delta.normalized * Mathf.Pow(1 + (dist - preferedDistanceToGroupMember) / preferedDistanceToGroupMember, 2) * distanceingRuleWeight;
            }

            walkingDir += agent.lookingDir * sameDirRuleWeight;




        }

        walkingDir.x = Mathf.Abs(walkingDir.x) < 0.01f ? 0 : walkingDir.x;
        walkingDir.y = Mathf.Abs(walkingDir.y) < 0.01f ? 0 : walkingDir.y;



        if (walkingDir != Vector2.zero && Physics2D.CircleCastAll(transform.position, radius, walkingDir.normalized, speed).Length == 1)
        {
            float movement = walkingDir.magnitude < speed ? walkingDir.magnitude : speed;


            transform.position += (Vector3)walkingDir.normalized * movement;
            transform.localEulerAngles = (Mathf.Asin((walkingDir.normalized - Vector2.right).magnitude / 2) * 2 * 180 / Mathf.PI * (walkingDir.normalized.y > 0 ? -1 : 1)) * Vector3.back;
            lookingDir = walkingDir.normalized;


        }

    }

    private void UpdateSenses()
    {
        RaycastHit2D[] inHearingRange = Physics2D.CircleCastAll(transform.position, hearingRadius, Vector2.zero);
        foreach (var heard in inHearingRange)
        {
            if (heard.transform != transform)
            {
                GroupAI ai = heard.transform.GetComponent<GroupAI>();
                if (ai != null && !agentsInRange.Contains(ai))
                    agentsInRange.Add(ai);
            }

        }

        RaycastHit2D[] inVisionRange = Physics2D.CircleCastAll(transform.position, visionRadius, Vector2.zero);
        foreach (var seen in inVisionRange)
        {
            if (seen.transform != transform)
            {
                Vector2 delta = (seen.transform.position - transform.position).normalized;
                bool inVisionCone = (Mathf.Asin((delta - lookingDir).magnitude / 2) * 2 * 180 / Mathf.PI) <= visionConeDegrees/2;
                
                if (inVisionCone)
                {

                    GroupAI ai = seen.transform.GetComponent<GroupAI>();
                    if (ai != null && !agentsInRange.Contains(ai))
                        agentsInRange.Add(ai);
                }
            }

        }
    }

    void OnValidate()
    {
        GetComponent<RangeDrawer>().UpdateRanges(hearingRadius, visionRadius, visionConeDegrees);
    }

}
