﻿using UnityEngine;

public class HeadQuarters : Building
{
    public int maxHealth;
    int health;

    public void Damage(int amount)
    {
        health -= amount;
        if(health < 0)
        {
            Debug.Log("You Lose");
        }
    }
}

