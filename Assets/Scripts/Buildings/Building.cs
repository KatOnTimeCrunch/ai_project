﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Building : MonoBehaviour
{
    public bool sellable;
    public bool walkable;
    public int cost;
    public BuildingPlacer buildingPlacer;

    public virtual void SetOverlayActive(bool active) { }
}

