﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : Building
{
    public int range;
    public float fireRate;
    public Bullet bulletPrefab;
    public Transform barrelRotationPoint;
    public Transform bulletSpawnPoint;
    SpriteRenderer overlay;
    Vector2Int tilePos;
    float timeUntilNextFire;

    private void Start()
    {
        overlay = transform.GetChild(0).GetComponent<SpriteRenderer>();
        overlay.transform.localScale = Vector2.one * (1 + range * 2);
        tilePos = new Vector2Int((int)transform.localPosition.x, (int)transform.localPosition.y);
    }
    private void FixedUpdate()
    {
        List<Creep> creeps = buildingPlacer.creepManager.GetCreepsInBox(tilePos, range * Vector2Int.one * 2);
        Creep selectedCreep = null;
        if (creeps.Count > 0)
        {
            selectedCreep = creeps[0];
            Vector2 dir = (selectedCreep.transform.position - transform.position).normalized;
            barrelRotationPoint.transform.localEulerAngles = (Mathf.Atan2(dir.y, dir.x) * 180 / Mathf.PI - 90) * Vector3.forward ;


        }

        if (timeUntilNextFire > 0)
        {
            timeUntilNextFire -= Time.fixedDeltaTime;
        }
        else
        {
            if(selectedCreep != null)
            {
                Bullet bullet = Instantiate(bulletPrefab);
                bullet.transform.parent = buildingPlacer.creepManager.world.transform;
                bullet.target = selectedCreep;
                bullet.transform.position = bulletSpawnPoint.position;
                timeUntilNextFire = fireRate;
            }
            
        }
     

       


    }

    public override void SetOverlayActive(bool active)
    {
        overlay.gameObject.SetActive(active);
    }
}
