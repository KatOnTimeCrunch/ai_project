﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreepManager : MonoBehaviour
{
    public List<Building> spawners;
    public World world;
    public List<Creep> creepTypes;
    public SpriteRenderer pathOverlayPrefab;
    [Range(0.1f, 2)]
    public float distanceToTargetScoreMult = 1;
    public Dictionary<Vector2Int, Vector2Int> shortestPathToHeadQuarters;

    public bool WaveStarted { get { return creeps.Count > 0; } }

    List<Creep> creeps;
    List<SpriteRenderer> pathOverlay;

    //Temp for pathfind (but cached sense we will pathfind multiple times)
    Dictionary<Vector2Int, float> tileScore;
    List<Vector2Int> tilesToOpen;
    
    void Start()
    {
        shortestPathToHeadQuarters = new Dictionary<Vector2Int, Vector2Int>();
        tileScore = new Dictionary<Vector2Int, float>();
        tilesToOpen = new List<Vector2Int>();
        creeps = new List<Creep>();
        pathOverlay = new List<SpriteRenderer>();

        foreach (Building spawner in spawners)
        {
            world.SetTile(world.GetTilePos(spawner), spawner);
        }

        GeneratePathMap();
    }

    public void StartWave()
    {
        if (!WaveStarted)
        {
            Creep c = Instantiate(creepTypes[0]);
            c.transform.parent = transform;
            c.transform.position = spawners[1].transform.position;
            c.creepManager = this;
            creeps.Add(c);
        }
        
    }
    public void KillCreep(Creep creep)
    {
        creeps.Remove(creep);
        Destroy(creep.gameObject);
    }

    public bool CanTileBeBlocked(Vector2Int tilePos)
    {
        return !shortestPathToHeadQuarters.ContainsKey(tilePos) || CanGeneratePathWithBlockTile(tilePos);
    }

    public bool CanGeneratePathWithBlockTile(Vector2Int tilePos)
    {
        Dictionary<Vector2Int, Vector2Int> temp = new Dictionary<Vector2Int, Vector2Int>();
        GeneratePathMap(ref temp, tilePos);
        return temp != null;
    }
    public void GeneratePathMap()
    {
        shortestPathToHeadQuarters.Clear();
        GeneratePathMap(ref shortestPathToHeadQuarters);
        MakePathOverlay();
    }
    private void GeneratePathMap(ref Dictionary<Vector2Int, Vector2Int> shortestPathToHeadQuarters, Vector2Int? blockTile = null)
    {
        Vector2Int hQPos = world.GetTilePos(world.headQuarters);
        foreach (Building spawner in spawners)
        {

            Vector2Int spawnerPos = world.GetTilePos(spawner);

            tileScore.Clear();
            tilesToOpen.Clear();

            tileScore.Add(spawnerPos, Vector2Int.Distance(spawnerPos, hQPos) * distanceToTargetScoreMult);
            tilesToOpen.Add(spawnerPos);
            Vector2Int tile;
            while (tilesToOpen.Count > 0 && tilesToOpen[0] != hQPos)
            {
                tile = tilesToOpen[0];
                tilesToOpen.RemoveAt(0);
                OpenTile(tile, hQPos, blockTile);

            }

            tile = hQPos;
            if (!tileScore.ContainsKey(hQPos))
            {
                shortestPathToHeadQuarters = null;
                return;
            }
                
            for (int i = 0; i < tileScore[hQPos]; i++)
            {
                Vector2Int bestTilePos = Vector2Int.zero;
                float shortestDistanceToStart = float.PositiveInfinity;
                

                for (int x = -1; x < 2; x ++)
                {
                    for (int y = (x != 0 ? 0 : -1); y < 2; y += 2)
                    {
                        Vector2Int tileToCheck = new Vector2Int(x + tile.x, y + tile.y);
                        
                        if (tileScore.ContainsKey(tileToCheck))
                        {
                            float distToStart = (tileScore[tileToCheck] - Vector2Int.Distance(tileToCheck, hQPos) * distanceToTargetScoreMult);

                            if (tileScore[tileToCheck] > 0 && distToStart < shortestDistanceToStart)
                            {

                                bestTilePos = tileToCheck;
                                shortestDistanceToStart = distToStart;
                            }
                        }
                        
                    }
                }

                shortestPathToHeadQuarters[bestTilePos] = tile;
                tile = bestTilePos;

                if (tile == spawnerPos)
                    break;
            }
        }

        
        
    }

    private void OpenTile(Vector2Int tile, Vector2Int targetTile, Vector2Int? blockTile)
    {

        float tileDistanceFromStart = tileScore[tile] - Vector2Int.Distance(tile, targetTile) * distanceToTargetScoreMult;
        for (int x = -1; x < 2; x++)
        {
            for (int y = (x != 0 ? 0 : -1); y < 2; y += 2)
            {
                Vector2Int tileToCheck = new Vector2Int(x + tile.x, y + tile.y);
                if (!tileScore.ContainsKey(tileToCheck) && world.InBounds(tileToCheck) && (world.GetBuilding(tileToCheck) == null || world.GetBuilding(tileToCheck).walkable) && (blockTile == null || tileToCheck != blockTile))
                {
                    tileScore[tileToCheck] = tileDistanceFromStart + Vector2Int.Distance(tileToCheck, targetTile) * distanceToTargetScoreMult + 1;
                    for (int i = 0; i < tilesToOpen.Count + 1; i++)
                    {
                        if (i == tilesToOpen.Count)
                        {
                            if (!tilesToOpen.Contains(tileToCheck))
                                tilesToOpen.Add(tileToCheck);
                            break;
                        }

                        if (tileScore[tilesToOpen[i]] > tileScore[tileToCheck])
                        {
                            if (tilesToOpen.Contains(tileToCheck))
                                tilesToOpen.Remove(tileToCheck);
                            tilesToOpen.Insert(i, tileToCheck);
                            break;
                        }
                    }
                }

            }
        }
    }

    private void MakePathOverlay()
    {
       
        int i = 0;
        foreach (var path in shortestPathToHeadQuarters.Keys)
        {
            if (!spawners.Find(x=> world.GetTilePos(x) == path))
            {
                if (i >= pathOverlay.Count - 1)
                {
                    pathOverlay.Add(Instantiate(pathOverlayPrefab));
                    pathOverlay[i].transform.parent = world.transform;
                }

                pathOverlay[i].transform.localPosition = (Vector2)path;
                i++;
            }
           
        }

        int loops = pathOverlay.Count;
        for (int f = i; f < loops; f++)
        {
            Destroy(pathOverlay[i].gameObject);
            pathOverlay.RemoveAt(i);
        }
    }

    public List<Creep> GetCreepsInBox(Vector2Int tilePos, Vector2 size)
    {
        List<Creep> creepsInRange = new List<Creep>();
        foreach (var creep in creeps)
        {
            if(creep.transform.localPosition.x >= tilePos.x - size.x / 2 && creep.transform.localPosition.x <= tilePos.x + size.x / 2 &&
               creep.transform.localPosition.y >= tilePos.y - size.y / 2 && creep.transform.localPosition.y <= tilePos.y + size.y / 2)
            {
                creepsInRange.Add(creep);
            }
        }
        return creepsInRange;
    }
}
