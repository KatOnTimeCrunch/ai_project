﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingPlacer : MonoBehaviour
{
    public Color canPlaceColor;
    public Color canNotPlaceColor;
    public Color buildingSelectedColor;
    public World world;
    public CreepManager creepManager;
    public int currentCash;
    Building buildingPrefab;
    Vector2Int prevTilePos;
    SpriteRenderer spriteRend;
    Building selected;

    void Start()
    {
        spriteRend = GetComponent<SpriteRenderer>();
        spriteRend.enabled = false;
    }


    void Update()
    {
        if (spriteRend.enabled)
        {
            Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2Int tilePos = new Vector2Int(Mathf.RoundToInt(pos.x - 0.5f), Mathf.RoundToInt(pos.y - 0.5f));
            transform.position = tilePos + Vector2.one * 0.5f;
            if (tilePos != prevTilePos)
            {
                prevTilePos = tilePos;
                spriteRend.color = IsTilePlaceable(tilePos) && buildingPrefab.cost <= currentCash ? canPlaceColor : canNotPlaceColor;
            }

            if (Input.GetMouseButtonDown(0))
            {

                if (IsTilePlaceable(tilePos) && buildingPrefab.cost <= currentCash)
                {
                    if (world.GetBuilding(tilePos) != null && world.GetBuilding(tilePos).sellable)
                        currentCash += world.SellBuilding(tilePos);
                    Building building = Instantiate(buildingPrefab);
                    building.buildingPlacer = this;
                    world.SetTile(tilePos, building);
                    currentCash -= buildingPrefab.cost;
                }


            }

            if (Input.GetMouseButtonDown(1))
            {
                SetBuilding(null);
                return;
            }
        }
        else
        {
            if (Input.GetMouseButtonDown(0))
            {
                Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Vector2Int tilePos = new Vector2Int(Mathf.RoundToInt(pos.x - 0.5f), Mathf.RoundToInt(pos.y - 0.5f));
                if (selected != null)
                    selected.SetOverlayActive(false);
                selected = world.GetBuilding(tilePos);
                if (selected != null)
                    selected.SetOverlayActive(true);
            }

            if (Input.GetMouseButtonDown(1))
            {
                if (selected != null)
                {
                    selected.SetOverlayActive(false);
                    selected = null;
                }
                    
            }
        }
    }

    public void SetBuilding(Building buildingPrefab)
    {
        if (this.buildingPrefab != buildingPrefab)
            this.buildingPrefab = buildingPrefab;
        else
            this.buildingPrefab = null;

        spriteRend.enabled = this.buildingPrefab != null;

        Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2Int tilePos = new Vector2Int(Mathf.RoundToInt(pos.x - 0.5f), Mathf.RoundToInt(pos.y - 0.5f));
        prevTilePos = tilePos;

        if (spriteRend.enabled)
            spriteRend.color = IsTilePlaceable(tilePos) && buildingPrefab.cost <= currentCash ? canPlaceColor : canNotPlaceColor;

        if (selected != null)
        {
            selected.SetOverlayActive(false);
            selected = null;
        }

    }

    private bool IsTilePlaceable(Vector2Int tilePos)
    {
        if (world.InBounds(tilePos))
        {
            if (world.GetBuilding(tilePos) != null)
            {
                if (world.GetBuilding(tilePos).sellable)
                    return (!creepManager.WaveStarted && (buildingPrefab.walkable || creepManager.CanTileBeBlocked(tilePos))) || world.GetBuilding(tilePos).walkable == buildingPrefab.walkable;
            }
            else
            {
                return (!creepManager.WaveStarted && creepManager.CanTileBeBlocked(tilePos)) || buildingPrefab.walkable;
            }

        }
        return false;
    }
}
